<?php

namespace JyWxThird\Init\ReplyMsg;

use JyWxThird\Kernel\XML;

trait Image
{
    public function Image($MediaId)
    {
        exit(XML::build([
            'ToUserName'   => $this->message['FromUserName'],
            'FromUserName' => $this->message['ToUserName'],
            'CreateTime'   => time(),
            'MsgType'      => 'image',
            'Image'        => [
                'MediaId' => $MediaId,
            ],
        ]));
    }
}
