<?php

namespace JyWxThird\Init\ReplyMsg;

use JyWxThird\Kernel\XML;

trait Voice
{
    public function Voice($MediaId)
    {
        exit(XML::build([
            'ToUserName'   => $this->message['FromUserName'],
            'FromUserName' => $this->message['ToUserName'],
            'CreateTime'   => time(),
            'MsgType'      => 'voice',
            'Voice'        => [
                'MediaId'     => $MediaId,
            ],
        ]));
    }
}
