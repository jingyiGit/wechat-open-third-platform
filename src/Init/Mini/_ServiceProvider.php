<?php
// 小程序入口
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/nearby-poi/nearbyPoi.add.html

namespace JyWxThird\Init\Mini;

use JyWxThird\Kernel\Http;
use JyWxThird\BasicService\Common;

class _ServiceProvider extends Common
{
  use Auth;
  use Safe;
  use Qrcode;
  use Media;
  use Logistics;
  use SubscribeMessage;
  
  /**
   * 取用户手机号码
   * code换取用户手机号。 每个code只能使用一次，code的有效期为5min
   *
   * @param string $code
   * @return array|bool
   */
  public function getPhoneNumber($code)
  {
    $param = [
      'code' => $code,
    ];
    $url   = $this->domainUrl . "/wxa/business/getuserphonenumber?access_token=" . $this->get_access_token();
    Http::asJson();
    $param = json_encode($param, JSON_UNESCAPED_UNICODE);
    $res   = Http::httpPostRaw($url, $param);
    return $this->handleReturn($res, 'count');
  }
}
