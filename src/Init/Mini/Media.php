<?php
// 小程序素材
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/customer-message/customerServiceMessage.getTempMedia.html

namespace JyWxThird\Init\Mini;

use JyWxThird\Kernel\Http;

trait Media
{
  /**
   * 临时素材上传
   * 小程序目前不支持支持永久素材
   *
   * @param string $filePath 文件路径
   * @return false|void
   */
  public function mediaUpload($filePath)
  {
    if (!file_exists($filePath)) {
      $this->setError('要上传的文件不存在，请检查路径是否有误: ' . $filePath);
      return false;
    }
    
    $param = [
      'type'         => 'image',
      'access_token' => $this->get_access_token(),
    ];
    
    $res = Http::httpUpload($this->domainUrl . "/cgi-bin/media/upload", ['media' => $filePath], $param);
    return $this->handleReturn($res, 'media_id');
  }
  
  /**
   * 获取临时素材
   *
   * @param string $media_id 媒体ID
   * @return array|bool
   */
  public function mediaGet($media_id)
  {
    $url = $this->domainUrl . "/cgi-bin/media/get?access_token=" . $this->get_access_token() . '&media_id=' . $media_id;
    return Http::httpGet($url);
  }
}
