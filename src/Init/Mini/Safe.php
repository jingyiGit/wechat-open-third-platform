<?php
// 检测图片/文本/音频是否含有违法违规内容
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/sec-check/security.mediaCheckAsync.html

namespace JyWxThird\Init\Mini;

use JyWxThird\Kernel\Http;


trait Safe
{
  /**
   * 异步校验图片/音频是否含有违法违规内容 TODO 未测试
   * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/sec-check/security.mediaCheckAsync.html
   *
   * @param array $params
   * @return array|bool|mixed
   */
  public function safeCheckMedia($params)
  {
    $param = [
      'media_url'  => $params['media_url'],
      'media_type' => $params['media_type'],
      'version'    => 2,
      'openid'     => $params['openid'],
      'scene'      => $params['scene'],
    ];
    $url   = $this->domainUrl . "/wxa/media_check_async?access_token=" . $this->get_access_token();
    $res   = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }

  /**
   * 检测文本是否有违规 TODO 未测试
   * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/sec-check/security.msgSecCheck.html
   *
   * @param array $params
   * @return void
   */
  public function safeCheckText($params)
  {
    $param = [
      'content'    => $params['content'],
      'media_type' => $params['media_type'],
      'version'    => 2,
      'openid'     => $params['openid'],
      'scene'      => $params['scene'],
    ];
    if (isset($params['nickname'])) {
      $param['nickname'] = $params['nickname'];
    }
    if (isset($params['title'])) {
      $param['title'] = $params['title'];
    }
    if (isset($params['signature'])) {
      $param['signature'] = $params['signature'];
    }
    $url = $this->domainUrl . "/wxa/msg_sec_check?access_token=" . $this->get_access_token();
    $res = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }
}