<?php
// 二维码/小程序码

namespace JyWxThird\Init\Mini;

use JyWxThird\Kernel\Http;

trait Qrcode
{
  /**
   * 创建小程序码，永久有效，有数量限制
   *
   * @param int|string $path  扫码进入的小程序页面路径，最大长度 128 字节，不能为空；对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"，即可在 wx.getLaunchOptionsSync 接口中的 query 参数获取到 {foo:"bar"}。
   * @param int|string $width 二维码的宽度，单位 px。最小 280px，最大 1280px
   * @return mixed
   */
  public function qrcodeCreate($path, $width = 430)
  {
    $param = [
      'path'  => $path,
      'width' => $width,
    ];
    $url   = $this->domainUrl . "/cgi-bin/wxaapp/createwxaqrcode?access_token=" . $this->get_access_token();
    $res   = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }

  /**
   * 创建小程序码1，永久有效，有数量限制
   *
   * @param int|string $path       扫码进入的小程序页面路径，最大长度 128 字节，不能为空；对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"，即可在 wx.getLaunchOptionsSync 接口中的 query 参数获取到 {foo:"bar"}。
   * @param int|string $width      二维码的宽度，单位 px。最小 280px，最大 1280px
   * @param bool       $auto_color 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调
   * @param array      $line_color auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
   * @param bool       $is_hyaline 是否需要透明底色，为 true 时，生成透明底色的小程序码
   * @return mixed
   */
  public function qrcodeCreate1($path, $width = 430, $auto_color = false, $line_color = [], $is_hyaline = false)
  {
    $param = [
      'path'       => $path,
      'width'      => $width,
      'auto_color' => $auto_color,
      'is_hyaline' => $is_hyaline,
    ];
    if ($line_color) {
      $param['line_color'] = $line_color;
    }
    $url = $this->domainUrl . "/wxa/getwxacode?access_token=" . $this->get_access_token();
    $res = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }

  /**
   * 创建小程序码，永久有效，数量暂无限制
   * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/qr-code/wxacode.getUnlimited.html
   *
   * @param array $params
   * @return array|bool|mixed
   */
  public function qrcodeCreate2($params)
  {
    $param = [
      'scene'       => $params['scene'],
      'page'        => $params['page'],
      'check_path'  => isset($params['check_path']) ? $params['check_path'] : true,
      'env_version' => isset($params['env_version']) ? $params['env_version'] : 'release',
      'width'       => isset($params['width']) ? $params['width'] : 430,
      'auto_color'  => isset($params['auto_color']) ? $params['auto_color'] : false,
      'is_hyaline'  => isset($params['is_hyaline']) ? $params['is_hyaline'] : false,
    ];
    if (isset($params['line_color'])) {
      $param['line_color'] = $params['line_color'];
    }

    $url = $this->domainUrl . "/wxa/getwxacodeunlimit?access_token=" . $this->get_access_token();
    $res = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }

  /**
   * 创建小程序Scheme码，适用于短信、邮件、外部网页、微信内等拉起小程序的业务场景
   *
   * @param array $params
   * @return array|bool|mixed
   */
  public function qrcodeCreateScheme($params = [])
  {
    $param = [
      'is_expire'       => isset($params['is_expire']) ? $params['is_expire'] : false,
      'expire_type'     => isset($params['expire_type']) ? $params['expire_type'] : 0,
      'expire_interval' => isset($params['expire_interval']) ? $params['expire_interval'] : false,
    ];
    if (isset($params['jump_wxa'])) {
      $param['jump_wxa'] = $params['jump_wxa'];
    }
    if (isset($params['expire_time'])) {
      $param['expire_time'] = $params['expire_time'];
    }
    $url = $this->domainUrl . "/wxa/generatescheme?access_token=" . $this->get_access_token();
    $res = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }

  /**
   * 查询小程序 scheme 码，及长期有效 quota。
   *
   * @param string $scheme
   * @return array|bool|mixed
   */
  public function queryScheme($scheme)
  {
    $param = [
      'scheme' => $scheme,
    ];
    $url   = $this->domainUrl . "/wxa/queryscheme?access_token=" . $this->get_access_token();
    $res   = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }

  /**
   * 创建小程序跳转链接，适用于短信、邮件、网页、微信内等拉起小程序的业务场景
   * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/url-link/urllink.generate.html
   *
   * @param $params
   * @return array|bool|mixed
   */
  public function qrcodeCreateLink($params = [])
  {
    $param = [
      'env_version' => isset($params['env_version']) ? $params['env_version'] : 'release',
      'is_expire'   => isset($params['is_expire']) ? $params['is_expire'] : false,
      'expire_type' => isset($params['expire_type']) ? $params['expire_type'] : 0,
    ];
    if (isset($params['path'])) {
      $param['path'] = $params['path'];
    }
    if (isset($params['query'])) {
      $param['query'] = $params['query'];
    }
    if (isset($params['expire_time'])) {
      $param['expire_time'] = $params['expire_time'];
    }
    if (isset($params['expire_interval'])) {
      $param['expire_interval'] = $params['expire_interval'];
    }
    if (isset($params['cloud_base'])) {
      $param['cloud_base'] = $params['cloud_base'];
    }
    $url = $this->domainUrl . "/wxa/generate_urllink?access_token=" . $this->get_access_token();
    $res = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }

  /**
   * 查询小程序 跳转链接配置 及 长期有效 quota。
   *
   * @param string $url_link
   * @return array|bool|mixed
   */
  public function queryLink($url_link)
  {
    $param = [
      'url_link' => $url_link,
    ];
    $url   = $this->domainUrl . "/wxa/query_urllink?access_token=" . $this->get_access_token();
    $res   = Http::httpPostJson($url, $param);
    if (!isset($res['errcode'])) {
      return $res;
    }
    return $this->handleReturn($res);
  }
}
