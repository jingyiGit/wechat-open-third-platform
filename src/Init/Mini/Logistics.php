<?php
// 物流助手
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/express/by-business/logistics.getPath.html

namespace JyWxThird\Init\Mini;

use JyWxThird\Kernel\Http;

trait Logistics
{
  /**
   * 获取支持的快递公司列表
   *
   * @return array|bool
   */
  public function logisticsGetAllDelivery()
  {
    $url = $this->domainUrl . "/cgi-bin/express/business/delivery/getall?access_token=" . $this->get_access_token();
    $res = Http::httpGet($url);
    return $this->handleReturn($res, 'data');
  }
  
  /**
   * 未完成测试
   * @return array|bool
   */
  public function logisticsGetPath()
  {
    $param = [
      'order_id'    => '4we55w41e55sdrqw454',
      'openid'      => 'odKtE5eQh9DLd3OWX8hqDok-JjcA',
      'delivery_id' => 'YUNDA',
      'waybill_id'  => '43237628103801',
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/getcategoriesbytype?access_token=' . $this->get_access_token(), $param);
    return $this->handleReturn($res, 'categories_list');
  }
}
