<?php
// 授权相关，如：小程序登录

namespace JyWxThird\Init\Mini;

use JyWxThird\Kernel\Http;

trait Auth
{
  /**
   * 小程序登录，获得临时登录凭证
   * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html
   *
   * @param string $ids   类目 id，多个用逗号隔开
   * @param int    $page  页码，默认为1
   * @param int    $limit 每页的数量，默认为30
   */
  public function authCode2Session($ids, $page = 1, $limit = 30)
  {
    $param = [
      'access_token' => $this->get_access_token(),
      'ids'          => $ids,
      'start'        => ($page - 1) * $limit,
      'limit'        => $limit,
    ];
    $res   = Http::httpGet($this->domainUrl . "/wxaapi/newtmpl/getpubtemplatetitles", $param);
    return $this->handleReturn($res, 'count');
  }
}
