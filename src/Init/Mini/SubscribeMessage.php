<?php
// 订阅消息
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.getCategory.html

namespace JyWxThird\Init\Mini;

use JyWxThird\Kernel\Http;

trait SubscribeMessage
{
  /**
   * 取小程序账号的类目
   *
   * @return array|bool
   */
  public function smTemplateGetCategory()
  {
    $params = [
      'access_token' => $this->get_access_token(),
    ];
    $res    = Http::httpGet($this->domainUrl . "/wxaapi/newtmpl/getcategory?" . http_build_query($params));
    return $this->handleReturn($res, 'data');
  }
  
  /**
   * 取帐号所属类目下的公共模板标题
   *
   * @return array|bool
   */
  public function smTemplateGetTitle($param)
  {
    if (!isset($param['page'])) {
      $param['page'] = 1;
    }
    if (!isset($param['limit'])) {
      $param['limit'] = 30;
    }
    $params = array_merge([
      'access_token' => $this->get_access_token(),
      'start'        => ($param['page'] - 1) * $param['limit'],
    ], $param);
    unset($params['page']);
    $res = Http::httpGet($this->domainUrl . "/wxaapi/newtmpl/getpubtemplatetitles?" . http_build_query($params));
    return $this->handleReturn($res, 'data');
  }
  
  /**
   * 取指定模板的详情
   *
   * @param string $tid 模板标题 id，可通过接口获取
   * @return array|bool
   */
  public function smTemplateGetDetailsByTid($tid)
  {
    $params = [
      'access_token' => $this->get_access_token(),
      'tid'          => $tid,
    ];
    $res    = Http::httpGet($this->domainUrl . "/wxaapi/newtmpl/getpubtemplatekeywords?" . http_build_query($params));
    return $this->handleReturn($res, 'data');
  }
  
  /**
   * 添加订阅消息模板
   *
   * @param array $params
   * @return array|bool|mixed
   */
  public function smTemplateAdd($params)
  {
    $params = json_encode($params, JSON_UNESCAPED_UNICODE);
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . "/wxaapi/newtmpl/addtemplate?access_token=" . $this->get_access_token(), $params);
    
    $this->coverError(200012, '个人模版数已达上限，上限25个');
    return $this->handleReturn($res, 'priTmplId');
  }
  
  /**
   * 取当前帐号下的个人模板列表
   *
   * @return array|bool
   */
  public function smTemplateGetList()
  {
    $params = [
      'access_token' => $this->get_access_token(),
    ];
    $res    = Http::httpGet($this->domainUrl . "/wxaapi/newtmpl/gettemplate?" . http_build_query($params));
    return $this->handleReturn($res, '__rpcCount');
  }
  
  /**
   * 删除帐号下的个人模板
   *
   * @param string $priTmplId 要删除的模板id
   * @return array|bool
   */
  public function smTemplateDel($priTmplId)
  {
    $params = [
      'priTmplId' => $priTmplId,
    ];
    $res    = Http::httpPostJson($this->domainUrl . "/wxaapi/newtmpl/deltemplate?access_token=" . $this->get_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 发送订阅消息
   *
   * @param array $params
   * @return array|bool
   */
  public function smTemplateSend($params)
  {
    // 处理兼容data
    foreach ($params['data'] as $key => &$v) {
      if (!isset($v['value'])) {
        $v = ['value' => $v];
      }
    }
    
    $params = json_encode($params, JSON_UNESCAPED_UNICODE);
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . "/cgi-bin/message/subscribe/send?access_token=" . $this->get_access_token(), $params);
    return $this->handleReturn($res);
  }
}
