<?php
// 小程序代码管理
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/code/commit.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniCodeManage
{
  /**
   * 上传小程序代码并生成体验版
   *
   * @param array $params
   * @return array|bool
   */
  public function miniCodeUpload($params)
  {
    Http::asJson();
    $params = json_encode($params, JSON_UNESCAPED_UNICODE);
    $res    = Http::httpPostRaw($this->domainUrl . '/wxa/commit?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 取体验版二维码
   *
   * @param string $path 指定二维码扫码后直接进入指定页面并可同时带上参数）
   * @return array|bool
   */
  public function miniCodeGetQrcode($path = '')
  {
    $params = [
      'access_token' => $this->get_authorizer_access_token(),
    ];
    if (isset($path)) {
      $params['path'] = $path;
    }
    $res = Http::httpGet($this->domainUrl . "/wxa/get_qrcode?" . http_build_query($params));
    if (isset($res['errcode'])) {
      return $this->handleReturn($res);
    }
    return $res;
  }
  
  /**
   * 取已上传的代码的页面列表
   *
   * @return array|bool
   */
  public function miniCodeGetPage()
  {
    $params = [
      'access_token' => $this->get_authorizer_access_token(),
    ];
    $res    = Http::httpGet($this->domainUrl . "/wxa/get_page?" . http_build_query($params));
    return $this->handleReturn($res, 'page_list');
  }
  
  /**
   * 提交审核
   *
   * @param array $params
   * @return array|bool
   */
  public function miniCodeSubmitAudit($params = [])
  {
    Http::asJson();
    if (!$params) {
      $params = '{}';
    } else {
      $params = json_encode($params, JSON_UNESCAPED_UNICODE);
    }
    $res = Http::httpPostRaw($this->domainUrl . '/wxa/submit_audit?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'auditid');
  }
  
  /**
   * 加急审核申请
   * 有加急次数的第三方可以通过该接口，对已经提审的小程序进行加急操作，加急后的小程序预计2-12小时内审完。但是，若代码中包含较复杂逻辑或其他特殊情况，可能会导致审核时间延长。
   *
   * @param int $auditid
   * @return array|bool
   */
  public function miniCodeGetUrgentAudit($auditid)
  {
    $params = [
      'auditid' => $auditid,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/speedupaudit?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 撤回审核
   *
   * @return array|bool
   */
  public function miniCodeCancelAudit()
  {
    $res = Http::httpGet($this->domainUrl . '/wxa/undocodeaudit?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res);
  }
  
  /**
   * 发布已通过审核的小程序
   *
   * @return array|bool
   */
  public function miniCodeRelease()
  {
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . '/wxa/release?access_token=' . $this->get_authorizer_access_token(), '{}');
    return $this->handleReturn($res);
  }
  
  /**
   * 版本回退
   *
   * @param int $app_version 默认是回滚到上一个版本；也可回滚到指定的小程序版本，可通过get_history_version获取app_version
   * @return array|bool
   */
  public function miniCodeBack($app_version)
  {
    $res = Http::httpGet($this->domainUrl . '/wxa/release?access_token=' . $this->get_authorizer_access_token() . 'app_version=' . app_version);
    return $this->handleReturn($res);
  }
  
  /**
   * 查询指定发布审核单的审核状态
   *
   * @param int $auditid
   * @return array|bool
   */
  public function miniCodeGetAuditStatus($auditid)
  {
    $params = [
      'auditid' => $auditid,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/get_auditstatus?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'status');
  }
  
  /**
   * 查询最新一次提交的审核状态
   *
   * @return array|bool
   */
  public function miniCodeGetLastAuditStatus()
  {
    $res = Http::httpGet($this->domainUrl . '/wxa/get_latest_auditstatus?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'auditid');
  }
  
  /**
   * 获取可回退的小程序版本
   *
   * @return array|bool
   */
  public function miniCodeGetHistoryVersion()
  {
    $res = Http::httpGet($this->domainUrl . '/wxa/revertcoderelease?action=get_history_version&access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'version_list');
  }
  
  /**
   * 修改小程序服务状态
   *
   * @param string $action 设置可访问状态，发布后默认可访问，close 为不可见，open 为可见
   * @return array|bool
   */
  public function miniCodeChangeVisitStatus($action)
  {
    $params = [
      'action' => $action,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/change_visitstatus?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 查询当前设置的最低基础库版本及各版本用户占比
   *
   * @return array|bool
   */
  public function miniCodeGetWeAppSupportVersion()
  {
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . '/cgi-bin/wxopen/getweappsupportversion?access_token=' . $this->get_authorizer_access_token(), '{}');
    return $this->handleReturn($res, 'now_version');
  }
  
  /**
   * 设置最低基础库版本
   *
   * @param string $version 为已发布的基础库版本号
   * @return array|bool
   */
  public function miniCodeSetWeAppSupportVersion($version)
  {
    $params = [
      'version' => $version,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/setweappsupportversion?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 查询服务商的当月提审限额（quota）和加急次数
   * 服务商可以调用该接口，查询当月平台分配的提审限额和剩余可提审次数，以及当月分配的审核加急次数和剩余加急次数。（所有旗下小程序共用该额度）
   *
   * @return array|bool
   */
  public function miniCodeGetQuota()
  {
    $res = Http::httpGet($this->domainUrl . '/wxa/queryquota?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'rest');
  }
  
  /**
   * 分阶段发布
   * 发布小程序接口 是全量发布，会影响到现网的所有用户。
   * 而本接口是创建一个灰度发布的计划，可以控制发布的节奏，避免一上线就影响到所有的用户。
   * 可以多次调用本次接口，将灰度的比例（gray_percentage）逐渐增大
   *
   * @param array $params
   * @return array|bool
   */
  public function miniCodeGrayRelease($params)
  {
    $res = Http::httpPostJson($this->domainUrl . '/wxa/grayrelease?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 取消分阶段发布
   * 在小程序分阶段发布期间，可以随时调用本接口取消分阶段发布。
   * 取消分阶段发布后，受影响的微信用户（即被灰度升级的微信用户）的小程序版本将回退到分阶段发布前的版本
   *
   * @return array|bool
   */
  public function miniCodeCancelGrayRelease()
  {
    $res = Http::httpGet($this->domainUrl . '/wxa/revertgrayrelease?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res);
  }
  
  /**
   * 查询当前分阶段发布详情
   *
   * @return array|bool
   */
  public function miniCodeGetGrayRelease()
  {
    $res = Http::httpGet($this->domainUrl . '/wxa/getgrayreleaseplan?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'gray_release_plan');
  }
}
