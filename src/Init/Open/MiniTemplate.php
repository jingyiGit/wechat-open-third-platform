<?php
// 小程序模板管理
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/ThirdParty/code_template/gettemplatedraftlist.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniTemplate
{
  /**
   * 获取代码草稿列表
   *
   * @param string $component_access_token
   * @return false|mixed
   */
  public function miniTemplateGetDraft($component_access_token)
  {
    $res = Http::httpGET($this->domainUrl . '/wxa/gettemplatedraftlist?access_token=' . $component_access_token);
    if (isset($res['draft_list'])) {
      return $res;
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 将草稿添加到代码模板库
   *
   * @param string $component_access_token
   * @param string $draft_id      草稿 ID
   * @param int    $template_type 默认值是0，对应普通模板；可选1，对应标准模板库，关于标准模板库和普通模板库的区别可以查看小程序模板库介绍
   * @return false|mixed
   */
  public function miniTemplateAdd($component_access_token, $draft_id, $template_type = 0)
  {
    $param = [
      'draft_id'      => $draft_id,
      'template_type' => $template_type,
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/wxa/addtotemplate?access_token=' . $component_access_token, $param);
    return $this->handleReturn($res);
  }
  
  /**
   * 获取代码模板列表
   *
   * @param string $component_access_token
   * @param int    $template_type 默认为0（对应普通模板）和1（对应标准模板），如果不填，则返回全部的。关于标准模板和普通模板的区别可查看小程序模板库介绍
   * @return false|mixed
   */
  public function miniTemplateGet($component_access_token, $template_type = 0)
  {
    $res = Http::httpGET($this->domainUrl . '/wxa/gettemplatelist?access_token=' . $component_access_token . '&template_type=' . $template_type);
    return $this->handleReturn($res, 'template_list');
  }
  
  /**
   * 删除指定代码模板
   *
   * @param string $component_access_token
   * @param string $template_id 要删除的模板 ID ，可通过获取代码模板列表接口获取。
   * @return false|mixed
   */
  public function miniTemplateDel($component_access_token, $template_id)
  {
    $param = [
      'template_id' => $template_id,
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/wxa/deletetemplate?access_token=' . $component_access_token, $param);
    return $this->handleReturn($res);
  }
}
