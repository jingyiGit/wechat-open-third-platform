<?php
// 小程序类目管理
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/category/addcategory.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniCategory
{
  /**
   * 获取可设置的所有类目
   *
   * @return array|bool
   */
  public function miniCategoryAll()
  {
    $res = Http::httpGET($this->domainUrl . '/cgi-bin/wxopen/getallcategories?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'categories_list');
  }
  
  /**
   * 获取已设置的所有类目
   *
   * @return array|bool
   */
  public function miniCategoryUseAll()
  {
    $res = Http::httpGET($this->domainUrl . '/cgi-bin/wxopen/getcategory?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'categories');
  }
  
  /**
   * 获取不同主体类型的类目
   *
   * @param int $verify_type 留空为0；个人主体是0；企业主体是1；政府是2；媒体是3；其他组织是4
   * @return array|bool
   */
  public function miniCategoryByType($verify_type = 0)
  {
    $params = [
      'verify_type' => $verify_type,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/getcategoriesbytype?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'categories_list');
  }
  
  /**
   * 获取审核时可填写的类目信息
   *
   * @return array|bool
   */
  public function miniCategoryAuditAll()
  {
    $res = Http::httpGET($this->domainUrl . '/cgi-bin/wxopen/getallcategories?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'categories_list');
  }
  
  /**
   * 添加类目
   *
   * @param array $categories 类目信息列表
   * @return array|bool
   */
  public function miniCategoryAdd($categories)
  {
      if(isset($categories['certicates']) && $categories['certicates']){
          foreach ($categories['certicates'] as &$item){
              // 如果传入的是文件，自动上传并获取到 media_id
              if (file_exists($item['value'])) {
                  if (!$media_id = $this->miniMediaUpload($this->get_authorizer_access_token(), $item['value'])) {
                      return false;
                  } else {
                      $item['value'] = $media_id;
                  }
              }
          }
      }

    $params = [
      'categories' => $categories,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/addcategory?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 修改类目资质信息
   *
   * @param int   $first      一级类目 ID
   * @param int   $second     二级类目 ID
   * @param array $certicates 类目信息列表
   * @return array|bool
   */
  public function miniCategoryUpdate($first, $second, $certicates)
  {
      foreach ($certicates as &$item){
          // 如果传入的是文件，自动上传并获取到 media_id
          if (file_exists($item['value'])) {
              if (!$media_id = $this->miniMediaUpload($this->get_authorizer_access_token(), $item['value'])) {
                  return false;
              } else {
                  $item['value'] = $media_id;
              }
          }
      }
    $params = [
      'first'      => $first,
      'second'     => $second,
      'certicates' => $certicates,
    ];
      dd($params);
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/modifycategory?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 删除类目
   *
   * @param int $first  一级类目 ID
   * @param int $second 二级类目 ID
   * @return array|bool
   */
  public function miniCategoryDel($first, $second)
  {
    $params = [
      'first'  => $first,
      'second' => $second,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/deletecategory?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
}
