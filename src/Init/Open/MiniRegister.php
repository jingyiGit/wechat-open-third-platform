<?php
// 小程序注册
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Register_Mini_Programs/Fast_Registration_Interface_document.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniRegister
{
  /**
   * 注册企业小程序
   *
   * @param string $component_access_token 第三方平台令牌component_access_token
   * @param array  $params
   * @return array|bool
   */
  public function miniRegisterEnterprise($component_access_token, $params)
  {
    Http::asJson();
    $params = json_encode($params, JSON_UNESCAPED_UNICODE);
    $res    = Http::httpPostRaw($this->domainUrl . '/cgi-bin/component/fastregisterweapp?action=create&component_access_token=' . $component_access_token, $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 查询创建任务状态接口详情（企业）
   *
   * @param string $component_access_token 第三方平台令牌component_access_token
   * @param array  $params
   * @return array|bool
   */
  public function miniRegisterEnterpriseQueyr($component_access_token, $params)
  {
    Http::asJson();
    $params = json_encode($params, JSON_UNESCAPED_UNICODE);
    $res    = Http::httpPostRaw($this->domainUrl . '/cgi-bin/component/fastregisterweapp?action=search&component_access_token=' . $component_access_token, $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 注册个人小程序
   * https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Register_Mini_Programs/fastregisterpersonalweapp.html
   *
   * @param string $component_access_token 第三方平台令牌component_access_token
   * @param array  $params
   * @return array|bool
   */
  public function miniRegisterPersonal($component_access_token, $params)
  {
    Http::asJson();
    $params = json_encode($params, JSON_UNESCAPED_UNICODE);
    $res    = Http::httpPostRaw($this->domainUrl . '/wxa/component/fastregisterpersonalweapp?action=create&component_access_token=' . $component_access_token, $params);
    return $this->handleReturn($res, 'taskid');
  }
  
  /**
   * 查询创建任务状态接口详情（个人）
   *
   * @param string $component_access_token 第三方平台令牌component_access_token
   * @param string $taskid                 任务id
   * @return array|bool
   */
  public function miniRegisterPersonalQueyr($component_access_token, $taskid)
  {
    $params = [
      'taskid' => $taskid,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/component/fastregisterpersonalweapp?action=query&component_access_token=' . $component_access_token, $params);
    return $this->handleReturn($res, 'taskid');
  }
  
  /**
   * 复用公众号注册小程序 - 第1步:创建授权链接
   * 调用文档 http://docs.ijingyi.com/web/#/81/2841
   *
   * @param string $appid          公众号的 appid
   * @param string $redirect_uri   用户扫码授权后，MP 扫码页面将跳转到该地址(注:1.链接需 urlencode 2.Host 需和第三方平台在微信开放平台上面填写的登 录授权的发起页域名一致)
   * @param int    $copy_wx_verify 默认为1，是否复用公众号的资质进行微信认证(1:申请复用资质进行微信 认证 0:不申请)
   * @return string
   */
  public function miniRegisterCopyMpCreateAuthUrl($appid, $redirect_uri, $copy_wx_verify = 1)
  {
    $params = [
      'component_appid' => $this->config['appid'],
      'appid'           => $appid,
      'copy_wx_verify'  => $copy_wx_verify,
      'redirect_uri'    => $redirect_uri,
    ];
    return 'https://mp.weixin.qq.com/cgi-bin/fastregisterauth?' . http_build_query($params);
  }
  
  /**
   * 复用公众号注册小程序 - 第3步:使用公众号 ticket 换取 authorization_code
   *
   * @param string $ticket 公众号管理员扫码确认授权注册，并跳转回第三方平台会带上这个ticket
   */
  public function miniRegisterCopyMpGetAuthorizationCode($ticket)
  {
    $params = [
      'ticket' => $ticket,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/account/fastregister?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'appid');
  }
}
