<?php
// 将公众号或小程序 授权到开放平台
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Before_Develop/Authorization_Process_Technical_Description.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait Auth
{
  /**
   * 创建授权链接
   * https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Before_Develop/Authorization_Process_Technical_Description.html
   *
   * @param string $pre_auth_code    预授权码
   * @param string $redirect_uri     回调 URI
   * @param int    $auth_type        要授权的帐号类型：1 则商户点击链接后，手机端仅展示公众号、2 表示仅展示小程序，3 表示公众号和小程序都展示。
   * @param string $category_id_list 指定的权限集id列表，如果不指定，则默认拉取当前第三方账号已经全网发布的权限集列表。如需要指定单个权限集ID，写法为“category_id_list=99” ，如果有多个权限集，则权限集id与id之间用竖线隔开。
   * @param bool   $isPc             是否在PC端授权，默认true，false移动端
   * @return string
   */
  public function createAuthUrl($pre_auth_code, $redirect_uri, $auth_type = 3, $category_id_list = '', $isPc = true)
  {
    $param = [
      'component_appid' => $this->config['appid'],
      'pre_auth_code'   => $pre_auth_code,
      'redirect_uri'    => $redirect_uri,
      'auth_type'       => $auth_type,
    ];
    if ($category_id_list) {
      $param['category_id_list'] = $category_id_list;
    }
    if ($isPc) {
      return 'https://mp.weixin.qq.com/cgi-bin/componentloginpage?' . http_build_query($param);
    } else {
      return 'https://mp.weixin.qq.com/safe/bindcomponent?action=bindcomponent&no_scan=1&' . http_build_query($param) . '#wechat_redirect';
    }
  }
  
  /**
   * 使用授权码获取授权信息
   * https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/ThirdParty/token/authorization_info.html
   *
   * @param string $component_access_token 第三方平台component_access_token，不是authorizer_access_token
   * @param string $authorization_code     授权码, 会在授权成功时返回给第三方平台，详见第三方平台授权流程说明
   */
  public function get_authorization_info($component_access_token, $authorization_code)
  {
    $param = [
      'component_appid'    => $this->config['appid'],
      'authorization_code' => $authorization_code,
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/cgi-bin/component/api_query_auth?component_access_token=' . $component_access_token, $param);
    if (isset($res['authorization_info'])) {
      return $res;
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 获取授权方帐号基本信息
   * https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/ThirdParty/token/api_get_authorizer_info.html
   *
   * @param string $component_access_token 第三方平台component_access_token，不是authorizer_access_token
   * @param string $authorizer_appid       授权方 appid
   */
  public function get_authorizer_info($component_access_token, $authorizer_appid)
  {
    $param = [
      'component_appid'  => $this->config['appid'],
      'authorizer_appid' => $authorizer_appid,
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/cgi-bin/component/api_get_authorizer_info?component_access_token=' . $component_access_token, $param);
    if (isset($res['authorization_info'])) {
      return $res;
    }
    $this->setError($res);
    return false;
  }
}
