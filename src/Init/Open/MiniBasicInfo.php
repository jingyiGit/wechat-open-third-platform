<?php
// 小程序基础信息设置
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Mini_Program_Basic_Info/Mini_Program_Information_Settings.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniBasicInfo
{
  /**
   * 获取小程序基本信息
   *
   * @return array|bool
   */
  public function miniBasicGetInfo()
  {
    $res = Http::httpGET($this->domainUrl . '/cgi-bin/account/getaccountbasicinfo?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'appid');
  }
  
  /**
   * 查询公众号/小程序是否绑定open帐号
   *
   * @return array|bool
   */
  public function miniBasicHaveOpen()
  {
    $res = Http::httpGET($this->domainUrl . '/cgi-bin/open/have?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'have_open');
  }
  
  /**
   * 设置服务器域名
   *
   * @param array $params
   * @return array|bool
   */
  public function miniBasicSetDomain($params)
  {
    $res = Http::httpPostJson($this->domainUrl . '/wxa/modify_domain?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'requestdomain');
  }
  
  /**
   * 设置业务域名
   *
   * @param array $params
   * @return array|bool
   */
  public function miniBasicSetWebDomain($params)
  {
    $res = Http::httpPostJson($this->domainUrl . '/wxa/setwebviewdomain?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'webviewdomain');
  }
  
  /**
   * 设置名称
   *
   * @param array $params
   * @return array|bool
   */
  public function miniBasicSetName($params)
  {
    // 如果传入的是文件，自动上传并获取到 media_id
    if (file_exists($params['id_card'])) {
      if (!$media_id = $this->miniMediaUpload($this->get_authorizer_access_token(), $params['id_card'])) {
        return false;
      } else {
        $params['id_card'] = $media_id;
      }
    }
    
    Http::asJson();
    $params = json_encode($params, JSON_UNESCAPED_UNICODE);
    $res    = Http::httpPostRaw($this->domainUrl . '/wxa/setnickname?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'audit_id');
  }
  
  /**
   * 查询改名审核状态
   * 调用设置名称接口，如果需要审核，会返回审核单 id（audit_id）,使用本接口可以查询改名审核状态。
   *
   * @param int $audit_id 审核单 id，由设置名称接口返回
   * @return array|bool
   */
  public function miniBasicSetNameQuery($audit_id)
  {
    $params = [
      'audit_id' => $audit_id,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/api_wxa_querynickname?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'audit_id');
  }
  
  /**
   * 设置头像
   *
   * @param array $params
   * @return array|bool
   */
  public function miniBasicSetAvatar($params)
  {
    // 如果传入的是文件，自动上传并获取到 media_id
    if (file_exists($params['head_img_media_id'])) {
      if (!$media_id = $this->miniMediaUpload($this->get_authorizer_access_token(), $params['head_img_media_id'])) {
        return false;
      } else {
        $params['head_img_media_id'] = $media_id;
      }
    }
    
    if (!isset($params['x1'])) {
      $params['x1'] = "0";
    }
    if (!isset($params['y1'])) {
      $params['y1'] = "0";
    }
    if (!isset($params['x2'])) {
      $params['x2'] = 1;
    }
    if (!isset($params['y2'])) {
      $params['y2'] = 1;
    }
    $res = Http::httpPostJson($this->domainUrl . '/cgi-bin/account/modifyheadimage?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 微信认证名称检测
   * 检测微信认证的名称是否符合规则，该接口只允许通过api创建的小程序使用。
   *
   * @param string $nick_name 名称（昵称）
   * @return array|bool
   */
  public function miniBasicCheckName($nick_name)
  {
    $param = json_encode(['nick_name' => $nick_name], JSON_UNESCAPED_UNICODE);
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . '/cgi-bin/wxverify/checkwxverifynickname?access_token=' . $this->get_authorizer_access_token(), $param);
    return $this->handleReturn($res, 'hit_condition');
  }
  
  /**
   * 修改简介
   * 调用本接口可以修改功能介绍。
   *
   * @param string $signature 功能介绍（简介）
   * @return array|bool
   */
  public function miniBasicSetSignature($signature)
  {
    $params = json_encode([
      'signature' => $signature,
    ], JSON_UNESCAPED_UNICODE);
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . '/cgi-bin/account/modifysignature?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 查询搜索设置
   * 通过本接口可以查询小程序当前是否可被搜索。
   *
   * @return array|bool
   */
  public function miniBasicQuerySearch()
  {
    $res = Http::httpGet($this->domainUrl . '/wxa/getwxasearchstatus?access_token=' . $this->get_authorizer_access_token());
    return $this->handleReturn($res, 'status');
  }
  
  /**
   * 修改搜索设置
   * 通过本接口修改小程序是否可被搜索，可以先查询小程序当前的隐私设置再决定是否修改。
   *
   * @param int $status 1 表示不可搜索，0 表示可搜索
   * @return array|bool
   */
  public function miniBasicSetSearch($status)
  {
    $params = [
      'status' => $status,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/changewxasearchstatus?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
}
