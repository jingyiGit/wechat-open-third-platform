<?php
// 小程序扫码规则管理
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/qrcode/qrcodejumpadd.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniQrcodeRule
{
  /**
   * 获取已设置的二维码规则
   *
   * @return array|bool
   */
  public function miniQrcodeRuleGet()
  {
    $param = [
      'access_token' => $this->get_authorizer_access_token(),
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumpget?access_token=' . $this->get_authorizer_access_token(), $param);
    return $this->handleReturn($res, 'rule_list');
  }
  
  /**
   * 获取校验文件名称及内容
   *
   * @return array|bool
   */
  public function miniQrcodeRuleGetCheckContent()
  {
    $param = [
      'access_token' => $this->get_authorizer_access_token(),
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumpdownload?access_token=' . $this->get_authorizer_access_token(), $param);
    return $this->handleReturn($res, 'file_name');
  }
  
  /**
   * 增加或修改二维码规则
   *
   * @param array $params
   * @return array|bool
   */
  public function miniQrcodeRuleSet($params)
  {
    $res = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumpadd?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 删除已设置的二维码规则
   *
   * @param string $prefix
   * @return array|bool
   */
  public function miniQrcodeRuleDel($prefix)
  {
    $params = [
      'prefix' => $prefix,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumpdelete?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 发布二维码规则
   *
   * @param string $prefix
   * @return array|bool
   */
  public function miniQrcodeRuleRelease($prefix)
  {
    $params = [
      'prefix' => $prefix,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumppublish?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 将二维码长链接转成短链接（好像已废弃，测试无效）
   *
   * @param string $long_url 需要转换的长链接，支持http://、https://、weixin://wxpay 格式的url
   * @return array|bool
   */
  public function shorturl($long_url)
  {
    $param = [
      'action'   => 'long2short',
      'long_url' => $long_url,
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/cgi-bin/shorturl?access_token=' . $this->get_authorizer_access_token(), $param);
    return $this->handleReturn($res, 'short_url');
  }
}
