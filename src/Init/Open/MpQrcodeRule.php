<?php
// 公众号扫码打开小程序规则管理
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Official__Accounts/qrcode/qrcodejumpadd.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MpQrcodeRule
{
  /**
   * 获取服务号二维码规则
   *
   * @param array $params
   * @return array|bool
   */
  public function mpQrcodeRuleGet($params)
  {
    $res = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumpget?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'rule_list');
  }
  
  /**
   * 增加或修改二维码规则
   *
   * @param array $params
   * @return array|bool
   */
  public function mpQrcodeRuleSet($params)
  {
    if (!isset($params['path'])) {
      $params['path'] = 'pages/index/index';
    }
    if (!isset($params['is_edit'])) {
      $params['is_edit'] = 0;
    }
    $res = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumpadd?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 发布二维码规则
   *
   * @param string $prefix 服务号的带参的二维码url
   * @return array|bool
   */
  public function mpQrcodeRuleRelease($prefix)
  {
    $params = [
      'prefix' => $prefix,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumppublish?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 删除二维码规则
   *
   * @param string $prefix 服务号的带参的二维码url
   * @param string $appid  小程序appid
   * @return array|bool
   */
  public function mpQrcodeRuleDel($prefix, $appid)
  {
    $params = [
      'prefix' => $prefix,
      'appid'  => $appid,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/qrcodejumpdelete?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
}
