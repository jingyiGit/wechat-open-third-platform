<?php
// 获取小程序码
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/qrcode/getwxacodeunlimit.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniQrcode
{
  /**
   * 获取unlimited小程序码
   * 调用本 API 可以获取小程序码，适用于需要的码数量极多的业务场景。通过该接口生成的小程序码，永久有效，数量暂无限制。
   *
   * @param array $params
   * @return array|bool
   */
  public function miniQrcodeUnlimited($params)
  {
    return Http::httpPostJson($this->domainUrl . '/wxa/getwxacodeunlimit?access_token=' . $this->get_authorizer_access_token(), $params);
  }
  
  /**
   * 获取小程序码
   * 调用本 API 可以获取小程序码，适用于需要的码数量较少的业务场景。通过该接口生成的小程序码，永久有效，有数量限制。
   *
   * @param array $params
   * @return array|bool
   */
  public function miniQrcodeWxacode($params)
  {
    return Http::httpPostJson($this->domainUrl . '/wxa/getwxacode?access_token=' . $this->get_authorizer_access_token(), $params);
  }
  
  /**
   * 获取小程序二维码
   * 调用本 API 可以获取小程序码，适用于需要的码数量较少的业务场景。通过该接口生成的小程序码，永久有效，有数量限制。
   *
   * @param array $params
   * @return array|bool
   */
  public function miniQrcodeGet($params)
  {
    return Http::httpPostJson($this->domainUrl . '/cgi-bin/wxaapp/createwxaqrcode?access_token=' . $this->get_authorizer_access_token(), $params);
  }
}
