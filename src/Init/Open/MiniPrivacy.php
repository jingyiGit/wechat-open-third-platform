<?php
// 小程序隐私相关
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/privacy_config/set_privacy_setting.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniPrivacy
{
  /**
   * 配置小程序用户隐私保护指引
   *
   * @param array $owner_setting 收集方（开发者）信息配置
   * @param array $setting_list  要收集的用户信息配置，可选择的用户信息类型参考下方详情。当privacy_ver传2或者不传时，setting_list是必填；当privacy_ver传1时，该参数不可传，否则会报错
   * @param int   $privacy_ver   用户隐私保护指引的版本，1表示现网版本；2表示开发版。默认是2开发版。
   * @return array|bool
   */
  public function miniPrivacySet(array $owner_setting, array $setting_list = [], $privacy_ver = 2)
  {
    $param = json_encode([
      'owner_setting' => $owner_setting,
      'setting_list'  => $setting_list,
      'privacy_ver'   => $privacy_ver,
    ], JSON_UNESCAPED_UNICODE);
    
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . '/cgi-bin/component/setprivacysetting?access_token=' . $this->get_authorizer_access_token(), $param);
    return $this->handleReturn($res);
  }
  
  /**
   * 查询小程序用户隐私保护指引
   *
   * @param int $privacy_ver 用户隐私保护指引的版本，1表示现网版本；2表示开发版。默认是2开发版。
   */
  public function miniPrivacyGet($privacy_ver = 2)
  {
    $param = [
      'privacy_ver' => $privacy_ver,
    ];
    $res   = Http::httpPostJson($this->domainUrl . '/cgi-bin/component/getprivacysetting?access_token=' . $this->get_authorizer_access_token(), $param);
    return $this->handleReturn($res, 'code_exist');
  }
  
  /**
   * 上传小程序用户隐私保护指引
   *
   * @param string $filePath 文件路径，只支持传txt文件
   * @return array|bool
   */
  public function miniPrivacyUpload($filePath)
  {
    if (!file_exists($filePath)) {
      $this->setError('要上传的文件不存在，请检查路径是否有误: ' . $filePath);
      return false;
    }
    
    $param = [
      'access_token' => $this->get_authorizer_access_token(),
    ];
    
    $res = Http::httpUpload($this->domainUrl . "/cgi-bin/component/uploadprivacyextfile", ['file' => $filePath], $param);
    return $this->handleReturn($res, 'ext_file_media_id');
  }
}
