<?php
// 小程序成员管理
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Mini_Program_AdminManagement/Admin.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniTester
{
  /**
   * 绑定体验者
   *
   * @param int $wechatid 微信号
   * @return array|bool
   */
  public function miniTesterBind($wechatid)
  {
    $params = [
      'wechatid' => $wechatid,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/bind_tester?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'userstr');
  }
  
  /**
   * 解绑体验者
   *
   * @param int $wechatid 微信号
   * @return array|bool
   */
  public function miniTesterUnBind($wechatid)
  {
    $params = [
      'wechatid' => $wechatid,
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/unbind_tester?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'userstr');
  }
  
  /**
   * 获取体验者列表
   *
   * @return array|bool
   */
  public function miniTesterGetList()
  {
    $params = [
      'action' => 'get_experiencer',
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/wxa/memberauth?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'members');
  }
}
