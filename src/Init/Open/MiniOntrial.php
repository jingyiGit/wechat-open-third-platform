<?php
// 试用小程序
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/beta_Mini_Programs/fastregister.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniOntrial
{
  /**
   * 试用小程序 - 创建
   *
   * @param string $component_access_token 第三方平台令牌component_access_token
   * @param string $name                   小程序名称，昵称半自动设定，强制后缀“的体验小程序”。且该参数会进行关键字检查，如果命中品牌关键字则会报错。
   *                                       如遇到品牌大客户要用试用小程序，建议用户先换个名字，认证后再修改成品牌名。只支持4-30个字符
   * @param string $openid                 微信用户的openid（不是微信号），试用小程序创建成功后会默认将该用户设置为小程序管理员。
   * @return false
   */
  public function miniOntrialCreate($component_access_token, $name, $openid)
  {
    $param = [
      'name'   => $name,
      'openid' => $openid,
    ];
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . '/wxa/component/fastregisterbetaweapp?access_token=' . $component_access_token, json_encode($param, JSON_UNESCAPED_UNICODE));
    if (isset($res['authorize_url'])) {
      return $res;
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 试用小程序 - 改名
   *
   * @param string $name                    小程序名称，昵称半自动设定，强制后缀“的体验小程序”。且该参数会进行关键字检查，如果命中品牌关键字则会报错。 如遇到品牌大客户要用试用小程序，建议用户先换个名字，认证后再修改成品牌名。
   */
  public function miniOntrialRename( $name)
  {
    $param = [
      'name' => $name,
    ];
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . '/wxa/setbetaweappnickname?access_token=' . $this->get_authorizer_access_token(), json_encode($param, JSON_UNESCAPED_UNICODE));
    return $this->handleReturn($res);
  }
  
  /**
   * 试用小程序 - 快速认证
   *
   * @param array  $verify_info             企业法人认证需要的信息
   */
  public function miniOntrialVerify($verify_info)
  {
    $param = [
      'verify_info' => $verify_info,
    ];
    Http::asJson();
    $res = Http::httpPostRaw($this->domainUrl . '/wxa/verifybetaweapp?access_token=' . $this->get_authorizer_access_token(), json_encode($param, JSON_UNESCAPED_UNICODE));
    return $this->handleReturn($res);
  }
}
