<?php
// 违规和申诉管理
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/records/getillegalrecords.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniViolation
{
  /**
   * 获取小程序违规处罚记录
   *
   * @return array|bool
   */
  public function miniViolationGet($param = [])
  {
    if (!isset($param['start_time'])) {
      $param['start_time'] = time();
    }
    if (!isset($param['end_time'])) {
      $param['end_time'] = strtotime('+90 day');
    }
    $res = Http::httpPostJson($this->domainUrl . '/wxa/getillegalrecords?access_token=' . $this->get_authorizer_access_token(), $param);
    return $this->handleReturn($res, 'records');
  }
  
  /**
   * 获取小程序申诉记录
   *
   * @param string $illegal_record_id 违规处罚记录id（通过getillegalrecords接口返回的记录id）
   * @return array|bool
   */
  public function miniViolationGetIllegal($illegal_record_id)
  {
    $param = [
      'illegal_record_id' => $illegal_record_id,
    ];
    
    $res = Http::httpPostJson($this->domainUrl . '/wxa/getappealrecords?access_token=' . $this->get_authorizer_access_token(), $param);
    return $this->handleReturn($res, 'records');
  }
}
