<?php
// 入口

namespace JyWxThird\Init\Open;

use JyWxThird\WxThird;
use JyWxThird\Kernel\Http;
use JyWxThird\BasicService\Common;

class _ServiceProvider extends Common
{
  use \JyWxThird\Init\Open\Auth;
  use \JyWxThird\Init\Open\Token;
  use \JyWxThird\Init\Open\MiniUser;
  use \JyWxThird\Init\Open\MiniOntrial;
  use \JyWxThird\Init\Open\MiniTemplate;
  use \JyWxThird\Init\Open\MiniBasicInfo;
  use \JyWxThird\Init\Open\MiniQrcode;
  use \JyWxThird\Init\Open\MiniCategory;
  use \JyWxThird\Init\Open\MiniTester;
  use \JyWxThird\Init\Open\MiniQrcodeRule;
  use \JyWxThird\Init\Open\MiniViolation;
  use \JyWxThird\Init\Open\MiniRegister;
  use \JyWxThird\Init\Open\MiniCodeManage;
  use \JyWxThird\Init\Open\MiniPrivacy;
  use \JyWxThird\Init\Open\MpQrcodeRule;
  use \JyWxThird\Init\Open\MpMiniManage;
  
  /**
   * 查询rid信息
   *
   * @param string $rid 调用接口报错返回的rid
   * @return array|bool
   */
  public function getRidInfo($rid)
  {
    $params = [
      'rid' => $rid,
    ];
    return Http::httpPostJson($this->domainUrl . '/cgi-bin/openapi/rid/get?access_token=' . $this->get_authorizer_access_token(), $params);
  }
  
  /**
   * 调用小程序的临时素材上传
   *
   * @param string $authorizer_access_token
   * @param string $filePath
   * @return false|mixed
   */
  private function miniMediaUpload($authorizer_access_token, $filePath)
  {
    $third = WxThird::Init(['appid' => 1, 'secret' => '12345678123456781234567812345678']);
    $third->mini->set_access_token($authorizer_access_token);
    if (!$media = $third->mini->mediaUpload($filePath)) {
      $this->setError($third->mini->getError());
      return false;
    }
    return $media['media_id'];
  }
}
