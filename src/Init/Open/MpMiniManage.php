<?php
// 公众号 - 小程序管理
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Official__Accounts/Mini_Program_Management_Permission.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MpMiniManage
{
  /**
   * 获取公众号关联的小程序
   *
   * @return array|bool
   */
  public function mpMiniGet()
  {
    $params = [
      'access_token' => $this->get_authorizer_access_token(),
    ];
    $res    = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/wxamplinkget?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res, 'wxopens');
  }
  
  /**
   * 关联小程序
   *
   * @param array $params
   * @return array|bool
   */
  public function mpMiniLink($params)
  {
    $res = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/wxamplink?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
  
  /**
   * 解除关联小程序
   *
   * @param string $appid
   * @return array|bool
   */
  public function mpMiniUnLink($appid)
  {
    $params = [
      'appid' => $appid,
    ];
    $res = Http::httpPostJson($this->domainUrl . '/cgi-bin/wxopen/wxampunlink?access_token=' . $this->get_authorizer_access_token(), $params);
    return $this->handleReturn($res);
  }
}
