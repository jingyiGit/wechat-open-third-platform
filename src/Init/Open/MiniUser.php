<?php
// 小程序用户相关，如：登录
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/others/WeChat_login.html

namespace JyWxThird\Init\Open;

use JyWxThird\Kernel\Http;

trait MiniUser
{
  /**
   * 小程序登录
   *
   * @param string $component_access_token
   * @param array  $param
   * @return array|bool
   */
  public function miniLogin($component_access_token, $param)
  {
    $params = array_merge([
      'grant_type'             => 'authorization_code',
      'component_appid'        => $this->config['appid'],
      'component_access_token' => $component_access_token,
    ], $param);
    $res    = Http::httpGet($this->domainUrl . "/sns/component/jscode2session?" . http_build_query($params));
    return $this->handleReturn($res, 'openid');
  }
  
  /**
   * 支付后获取用户 Unionid
   *
   * @param array $params
   * @return array|bool
   */
  public function miniGetUnionid($params)
  {
    $res    = Http::httpGet($this->domainUrl . "/wxa/getpaidunionid?" . http_build_query($params));
    return $this->handleReturn($res, 'unionid');
  }
}
