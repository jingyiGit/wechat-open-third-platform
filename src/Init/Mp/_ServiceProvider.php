<?php
// 公众号入口

namespace JyWxThird\Init\Mp;

use JyWxThird\BasicService\Common;
use JyWxThird\Init\Mp\H5;
use JyWxThird\Init\Mp\Msg;
use JyWxThird\Init\Mp\Menu;
use JyWxThird\Init\Mp\Media;
use JyWxThird\Init\Mp\Qrcode;
use JyWxThird\Init\Mp\Newtmpl;
use JyWxThird\Init\Mp\Template;

class _ServiceProvider extends Common
{
  use H5;
  use Msg;
  use Menu;
  use Media;
  use Qrcode;
  use Newtmpl;
  use Template;
}
