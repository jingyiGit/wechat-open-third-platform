<?php
// 菜单相关

namespace JyWxThird\Init\Mp;

use JyWxThird\Kernel\Http;

trait Menu
{
  /**
   * 创建菜单
   *
   * @param array|string $body 支持数组，或JSON文本
   * @return false|string
   * @throws \Throwable
   */
  public function createMenu($body)
  {
    if (is_string($param) && $temp = json_decode($body, true)) {
      $body = $temp;
    }
    $url = $this->domainUrl . "/cgi-bin/menu/create?access_token=" . $this->get_access_token();
    return $this->handleReturn(Http::httpPostRaw($url, json_encode(['button' => $body], JSON_UNESCAPED_UNICODE)));
  }
  
  /**
   * 查询获取菜单
   *
   * @return mixed
   */
  public function getMenu()
  {
    $res = Http::httpGet($this->domainUrl . "/cgi-bin/get_current_selfmenu_info?access_token=" . $this->get_access_token());
    if (isset($res['selfmenu_info'])) {
      return $res;
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 删除菜单
   *
   * @return mixed
   */
  public function delMenu()
  {
    return $this->handleReturn(Http::httpGet($this->domainUrl . "/cgi-bin/menu/delete?access_token=" . $this->get_access_token()));
  }
}
