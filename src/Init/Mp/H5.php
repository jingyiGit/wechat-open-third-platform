<?php
// H5授权相关

namespace JyWxThird\Init\Mp;

use JyWxThird\Kernel\Http;

trait H5
{
  /**
   * 获取Ticket
   * https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html#62
   *
   * @return bool
   */
  public function h5GetTicket()
  {
    $url = $this->domainUrl . "/cgi-bin/ticket/getticket?access_token=" . $this->get_access_token() . '&type=jsapi';
    return $this->handleReturn(Http::httpGet($url), 'ticket');
  }
  
  /**
   * h5，获取初始化配置所需要的信息
   *
   * @param string $jsapi_ticket 通过 h5GetTicket() 函数获得
   * @param string $url          url（当前网页的URL，不包含#及其后面部分）
   * @return array
   */
  public function h5GetSign($jsapi_ticket, $url)
  {
    $param = [
      'noncestr'     => md5(uniqid(microtime(true), true)),
      'jsapi_ticket' => $jsapi_ticket,
      'timestamp'    => time(),
      'url'          => $url,
    ];
    ksort($param);
    $str = [];
    foreach ($param as $k => $v) {
      $str[] = "{$k}={$v}";
    }
    $param['sign'] = sha1(implode('&', $str));
    unset($param['jsapi_ticket']);
    return $param;
  }
}
