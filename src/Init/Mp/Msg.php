<?php
// 基础消息处理
// https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Getting_Rules_for_Auto_Replies.html

namespace JyWxThird\Init\Mp;

use JyWxThird\Kernel\Http;

trait Msg
{
  /**
   * 获取公众号的自动回复规则
   *
   * @return bool
   */
  public function msgGetAutoreplyInfo()
  {
    $url = $this->domainUrl . "/cgi-bin/get_current_autoreply_info?access_token=" . $this->get_access_token();
    return $this->handleReturn(Http::httpGet($url), 'is_add_friend_reply_open');
  }
}
