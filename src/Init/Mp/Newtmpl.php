<?php
// 订阅通知
// https://developers.weixin.qq.com/doc/offiaccount/Subscription_Messages/api.html

namespace JyWxThird\Init\Mp;

use JyWxThird\Kernel\Http;

trait Newtmpl
{
  /**
   * 获取公众号类目
   */
  public function newtmplGetCategory()
  {
    $url = $this->domainUrl . "/wxaapi/newtmpl/getcategory?access_token=" . $this->get_access_token();
    $res = Http::httpGet($url);
    return $this->handleReturn($res, 'data');
  }
  
  /**
   * 获取类目下的【公共模板】列表
   *
   * @param string $ids   类目 id，多个用逗号隔开
   * @param int    $page  页码，默认为1
   * @param int    $limit 每页的数量，默认为30
   */
  public function newtmplGetCommonList($ids, $page = 1, $limit = 30)
  {
    $param = [
      'access_token' => $this->get_access_token(),
      'ids'          => $ids,
      'start'        => ($page - 1) * $limit,
      'limit'        => $limit,
    ];
    $res   = Http::httpGet($this->domainUrl . "/wxaapi/newtmpl/getpubtemplatetitles", $param);
    return $this->handleReturn($res, 'count');
  }
  
  /**
   * 获取类目下的【私有模板】列表
   */
  public function newtmplGetPrivateList()
  {
    $param = [
      'access_token' => $this->get_access_token(),
    ];
    $res   = Http::httpGet($this->domainUrl . "/wxaapi/newtmpl/gettemplate", $param);
    return $this->handleReturn($res, 'data');
  }
  
  /**
   * 获取指定模板的关键词列表
   *
   * @param int $tid 模板标题 id，可通过接口获取
   */
  public function newtmplGetTemplateKeywords($tid)
  {
    $param = [
      'access_token' => $this->get_access_token(),
      'tid'          => $tid,
    ];
    $res   = Http::httpGet($this->domainUrl . "/wxaapi/newtmpl/getpubtemplatekeywords", $param);
    return $this->handleReturn($res, 'data');
  }
  
  /**
   * 选用模板
   *
   * @param int    $tid       模板 id
   * @param array  $kidList   开发者自行组合好的模板关键词列表，关键词顺序可以自由搭配（例如 [3,5,4] 或 [4,5,3]），最多支持5个，最少2个关键词组合
   * @param string $sceneDesc 服务场景描述，15个字以内
   */
  public function newtmplAdd($tid, $kidList, $sceneDesc)
  {
    if (count($kidList) < 2) {
      $this->setError('kidList 最少要求2个，如： [1,2]');
      return false;
    } elseif (count($kidList) > 5) {
      $this->setError('kidList 最多支持5个');
      return false;
    }
    $param = [
      'tid'       => $tid,
      'kidList'   => $kidList,
      'sceneDesc' => $sceneDesc,
    ];
    $url   = $this->domainUrl . "/wxaapi/newtmpl/addtemplate?access_token=" . $this->get_access_token();
    $res   = Http::httpPostJson($url, $param);
    return $this->handleReturn($res, 'priTmplId');
  }
  
  /**
   * 删除模板
   *
   * @param string $priTmplId 要删除的模板id，通过选用模板返回
   */
  public function newtmplDel($priTmplId)
  {
    $param = [
      'priTmplId' => $priTmplId,
    ];
    $url   = $this->domainUrl . "/wxaapi/newtmpl/deltemplate?access_token=" . $this->get_access_token();
    $res   = Http::httpPostJson($url, $param);
    return $this->handleReturn($res);
  }
  
  /**
   * 发送订阅通知
   *
   * @param array $param 参数
   */
  public function newtmplSend($param)
  {
    if (is_string($param)) {
      if (!$temp = json_decode($param, true)){
        $this->setError('你提供的JSON参数，格式有误，请检查');
        return false;
      }
      $param = $temp;
    }
    $url = $this->domainUrl . "/cgi-bin/message/subscribe/bizsend?access_token=" . $this->get_access_token();
    $res = Http::httpPostJson($url, $param);
    return $this->handleReturn($res);
  }
}
