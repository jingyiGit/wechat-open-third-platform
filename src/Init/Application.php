<?php

namespace JyWxThird\Init;

use JyWxThird\Kernel\Response;
use JyWxThird\BasicService\BaseConfig;
use JyWxThird\BasicService\ServiceContainer;

/**
 * Class Application.
 *
 * @property \JyWxThird\Init\Mp\_ServiceProvider   $mp
 * @property \JyWxThird\Init\Open\_ServiceProvider $open
 * @property \JyWxThird\Init\Mini\_ServiceProvider $mini
 * @property \JyWxThird\Init\Call\_ServiceProvider $call
 */
class Application extends ServiceContainer
{
  use Response;
  use BaseConfig;
  
  /**
   * @var array
   */
  protected $providers = [
    'open' => Open\_ServiceProvider::class,
    'mp'   => Mp\_ServiceProvider::class,
    'mini' => Mini\_ServiceProvider::class,
    'call' => Call\_ServiceProvider::class,
  ];
  
  public function __construct(array $config = [])
  {
    parent::__construct($this->initConfig($config));
  }
}
