<?php

namespace JyWxThird\BasicService;

use Pimple\Container;
use JyWxThird\Kernel\Response;
use JyWxThird\BasicService\BaseConfig;

class ServiceContainer extends Container
{
  use Response;
  use BaseConfig;
  
  protected $config = [];
  
  /**
   * Constructor.
   *
   * @param array       $config
   * @param string|null $id
   */
  public function __construct(array $config = [])
  {
    $this->config = $this->initConfig($config);
    $this->registerProviders($this->getProviders());
  }
  
  /**
   * Magic get access.
   *
   * @param string $id
   *
   * @return mixed
   */
  public function __get($id)
  {
    return $this->offsetGet($id);
  }
  
  /**
   * Magic set access.
   *
   * @param string $id
   * @param mixed  $value
   */
  public function __set($id, $value)
  {
    $this->offsetSet($id, $value);
  }
  
  /**
   * Return all providers.
   *
   * @return array
   */
  public function getProviders()
  {
    return $this->providers;
  }
  
  /**
   * @param array $providers
   */
  public function registerProviders(array $providers)
  {
    foreach ($providers as $key => $provider) {
      $this[$key] = new $provider($this->config);
    }
  }
}
