<?php

namespace JyWxThird\BasicService;

trait BaseConfig
{
  /**
   * 初始化配置
   *
   * @param array  $config           配置
   * @return mixed
   */
  protected function initConfig($config)
  {
    // 检测必要配置项
    $this->checkConfig($config);
    return $config;
  }
  
  /**
   * 检测必要配置项
   *
   * @param $config
   */
  protected function checkConfig($config)
  {
    // appid
    if (!isset($config['appid']) || !$config['appid']) {
      $this->fail('appid 不能为空');
      
      // secret
    } else if (!isset($config['secret']) || strlen(trim($config['secret'])) != 32) {
      $this->fail('secret 为空或长度不对');
    }
  }
}
